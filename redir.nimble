# Package

version       = "0.1.0"
author        = "Rasmus Moorats"
description   = "redir.ee"
license       = "MIT"
srcDir        = "src"
binDir        = "bin"
bin           = @["redir"]


# Dependencies

requires "nim >= 2.0.0"
requires "httpbeast >= 0.4.1"
requires "chronicles >= 0.10.3"
requires "cligen >= 1.6.15"
requires "webby >= 0.1.9"
