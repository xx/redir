open redirector

usage:

  * /[url]      - redirect to an URL
  * /?url=[url] - redirect to an URL
  * /base64     - redirect to a base64-encoded URL

  Redirect to any location using the `Location` header.
  Supply the `code` query with a valid 3xx HTTP status
  code to use a specific one. Otherwise, `302 Found` is used


examples:

  * http://redir.ee/file:///etc/shadow
    - redirects to file:///etc/shadow

  * http://redir.ee/?url=http://example.com
    - redirects to http://example.com

  * http://redir.ee/base64?url=aHR0cDovL2V4YW1wbGUuY29t&code=303
    - redirects to http://example.com with HTTP code 303


pre-defined shortcuts (`code` works here too):

  * /metadata    -  http://169.254.169.254/latest/meta-data/
  * /metadata6   -  http://[fd00:ec2::254]/latest/meta-data/
  * /localhost   -  http://127.0.0.1
  * /localhost6  -  http://[::1]
  * /zeroes      -  http://0.0.0.0
  * /passwd      -  file:///etc/passwd
  * /services    -  file:///etc/services
  * /env         -  file:///proc/self/environ


source code: https://git.dog/xx/redir
