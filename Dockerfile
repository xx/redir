FROM docker.io/akito13/nim:2.0.0-alpine AS buildbase

WORKDIR /usr/src/app/
COPY . /usr/src/app/
RUN nimble -y build

FROM scratch

COPY --from=buildbase /usr/src/app/bin/redir /redir

ENTRYPOINT ["/redir"]
